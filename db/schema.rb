# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180520123345) do

  create_table "drivers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "license_no"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "drivers", ["email"], name: "index_drivers_on_email", unique: true
  add_index "drivers", ["license_no"], name: "index_drivers_on_license_no", unique: true

  create_table "ins_companies", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "riders", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email"
    t.string   "external_uid"
    t.string   "jwt_token"
    t.integer  "ins_provider_id"
    t.boolean  "tos_agreed"
    t.string   "provider"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "ins_card_photo_file_name"
    t.string   "ins_card_photo_content_type"
    t.integer  "ins_card_photo_file_size"
    t.datetime "ins_card_photo_updated_at"
  end

  add_index "riders", ["email"], name: "index_riders_on_email", unique: true
  add_index "riders", ["phone"], name: "index_riders_on_phone", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "phone"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
