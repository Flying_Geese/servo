class AddAttachmentInsCardPhotoToRiders < ActiveRecord::Migration
  def self.up
    change_table :riders do |t|
      t.attachment :ins_card_photo
    end
  end

  def self.down
    remove_attachment :riders, :ins_card_photo
  end
end
