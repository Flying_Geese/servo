class CreateRiders < ActiveRecord::Migration
  def change
    create_table :riders do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :external_uid
      t.string :jwt_token
      t.integer :ins_provider_id
      t.boolean :tos_agreed
      t.string :provider

      t.timestamps null: false
    end
    add_index :riders, :phone, unique: true
    add_index :riders, :email, unique: true
  end
end
