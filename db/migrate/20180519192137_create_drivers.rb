class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :license_no

      t.timestamps null: false
    end
    add_index :drivers, :email, unique: true
    add_index :drivers, :license_no, unique: true
  end
end
