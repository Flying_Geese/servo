class CreateInsCompanies < ActiveRecord::Migration
  def change
    create_table :ins_companies do |t|
      t.string :name
      t.string :phone

      t.timestamps null: false
    end
  end
end
