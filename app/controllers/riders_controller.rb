class RidersController < ApplicationController
  require 'curb'
  require 'json'
  before_action :set_rider, only: [:show, :edit, :update, :destroy]

  # GET /riders
  # GET /riders.json
  def index
    @riders = Rider.all
  end

  # GET /riders/1
  # GET /riders/1.json
  def show
  end

  # GET /riders/new
  def new
    @rider = Rider.new
  end

  # GET /riders/1/edit
  def edit
  end

  # POST /riders
  # POST /riders.json
  def create
    @rider = Rider.new(rider_params)

if @rider.save
  http = Curl.post("https://api.coord.co/v1/users/testing/user_and_jwt?access_key=pVf-tBnK6qy141e4ScSBVA7ZKZ9G98DzH-Hd9oyCkVo", rider_params[:email]) do |x|
    x.headers['Content-Type'] = 'application/json'
    x.headers['Accept']       = 'application/json'
  end
  p "HERE"
  p rider_params[:email]
  response = JSON.parse(http.body_str)
  p response
  @rider.jwt_token = response['jwt_token']
  @rider.save # again?
  p 'COORD account created'
  redirect_to @rider
else
  render 'new'
end

    # respond_to do |format|
    #   if @rider.save
    #     # create COORD user
    #     # c.http_post(Curl::PostField.content('user[email]', "#{@rider.email}"))
    #
    #     # attach COORD acct
    #     format.html { redirect_to @rider, notice: 'Rider was successfully created.' }
    #     format.json { render :show, status: :created, location: @rider }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @rider.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /riders/1
  # PATCH/PUT /riders/1.json
  def update
    respond_to do |format|
      if @rider.update(rider_params)
        format.html { redirect_to @rider, notice: 'Rider was successfully updated.' }
        format.json { render :show, status: :ok, location: @rider }
      else
        format.html { render :edit }
        format.json { render json: @rider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /riders/1
  # DELETE /riders/1.json
  def destroy
    @rider.destroy
    respond_to do |format|
      format.html { redirect_to riders_url, notice: 'Rider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rider
      @rider = Rider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rider_params
      params.require(:rider).permit(:first_name, :ins_card_photo, :last_name, :phone, :email, :ins_provider_id, :tos_agreed, :provider)
    end
end
