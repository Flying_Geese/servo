class Rider < ActiveRecord::Base
  has_attached_file :ins_card_photo,
                   styles: { normal: '180x180#', thumb: '32x32#', medium: '64x64#' }
  validates_attachment_content_type :ins_card_photo, :content_type => /\Aimage\/.*\Z/
end
