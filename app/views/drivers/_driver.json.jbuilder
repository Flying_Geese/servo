json.extract! driver, :id, :first_name, :last_name, :email, :password_digest, :license_no, :created_at, :updated_at
json.url driver_url(driver, format: :json)
