json.extract! rider, :id, :first_name, :last_name, :phone, :email, :ins_provider_id, :tos_agreed, :provider, :created_at, :updated_at
json.url rider_url(rider, format: :json)
